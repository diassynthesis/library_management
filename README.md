
  
 # Odoo Library Management System
 
 **This model  allows you to manage library book catalogue and lending.**


[Report Bug](https://gitlab.com/MalekShabab/library-management/issues)
| [Request Feature](https://gitlab.com/MalekShabab/library-management/issues)
| [Explore our models »](https://apps.odoo.com/apps/modules/browse?author=MALEK%20ABUSHABAB)



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)



<!-- ABOUT THE PROJECT -->
## About The Project



### Built With
* [Odoo](https://www.odoo.com) - Amazing business platform
* [Python](https://www.python.org) -  Beautiful programming language .
* [Visual Studio Code](https://code.visualstudio.com/) - Code editing. Redefined.


<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

This project is licensed under the LGPL v3 License - see the LICENSE.md file for details.



<!-- CONTACT -->
## Contact

Malik Abushabab  -  MALEK@MALEKSHABAB.TECH
