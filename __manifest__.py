{
    'name': 'Library Management',
    'description': 'Manage library book catalogue and lending.',
    'version': '12.0.0.1.0.0',
    'license': 'LGPL-3',
    'summary': 'Manage library book catalogue and lending.',
    'website': 'https://gitlab.com/MalekShabab/library-management',
    'category': 'Management',
    'author': 'Malik Abushabab',
    'depends': ['base','mail'],
    'data': [
        'security/library_security.xml',
        'security/ir.model.access.csv',
        'views/library_menu.xml',
        'views/book_view.xml',
        'views/book_list_template.xml',
        'views/member_view.xml',
    ],
    'application': True,
    'installable': True,
    'auto_install': True,
}