from odoo import fields, models, api
from odoo.exceptions import Warning
class Book(models.Model):

    def _check_isbn(self):
        digits = [int(x) for x in self.isbn if x.isdigit()]
        if len(digits) == 13:
            return self._check_isbn_13_digit()
        elif len(digits) == 10:
            return self._check_isbn_10_digit()

    @api.multi
    def _check_isbn_13_digit(self):
        self.ensure_one()
        digits = [int(x) for x in self.isbn if x.isdigit()]
        ponderations = [1, 3] * 6
        terms = [a * b for a, b in zip(digits[:12], ponderations)]
        remain = sum(terms) % 10
        check = 10 - remain if remain != 0 else 0
        return digits[-1] == check

    @api.multi
    def _check_isbn_10_digit(self):
        ''' some older titles might have a 10-digit ISBN.'''
        self.ensure_one()
        digits = [int(x) for x in self.isbn if x.isdigit()]
        ponderators = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        total = sum(a * b for a, b in zip(digits[:9], ponderators))
        check = total % 11
        return digits[-1] == check
        
            
    @api.multi
    def button_check_isbn(self):
        for book in self:
            if not book.isbn:
                raise Warning('Please provide an ISBN for %s' % book.name)
            if book.isbn and not book._check_isbn():
                raise Warning('%s is an invalid ISBN' % book.isbn)
        return True

    _name = 'library.book'
    _description = 'Book'
    name = fields.Char('Title', required=True)
    isbn = fields.Char('ISBN',help="Use a valid ISBN-13 or ISBN-10.")
    active = fields.Boolean('Active?', default=True)
    date_published = fields.Date()
    image = fields.Binary('Cover')
    publisher_id = fields.Many2one('res.partner', string='Publisher', index=True)
    author_ids = fields.Many2many('res.partner', string='Authors')
    is_available = fields.Boolean('Is Available?')
    